import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_database/firebase_database.dart';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'login.dart';

Future<void> main() async {
  final FirebaseApp app = await FirebaseApp.configure(
      name: 'db2',
      options: const FirebaseOptions(
        googleAppID: '1:878615028450:android:1ba66e71ee51eb14',
        apiKey: 'AIzaSyAhpltPTLLpRdZ34ldXKseRgnFcc6G4eyI',
      ));
   Firestore firestore = Firestore();
/*   await firestore.settings(timestampsInSnapshotsEnabled: true);
 */
  runApp(new Gnappa(app: app, firestore: firestore));
}

class Gnappa extends StatelessWidget {
  Gnappa({this.app, this.firestore});
  final FirebaseApp app;
  final Firestore firestore;
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Gnappa',
      theme: new ThemeData(
        primarySwatch: Colors.green,
      ),
      home: new Login(observer),
    );
  }
}
