import 'package:flutter/material.dart';
import 'package:firebase_analytics/observer.dart';

import 'productsTab.dart';
import 'profileTab.dart';

class Profile extends StatefulWidget {
  static const String routeName = '/profile';

  Profile(this.observer, this.phoneNumber);

  final FirebaseAnalyticsObserver observer;
  final String phoneNumber;

  @override
  State<StatefulWidget> createState() => _ProfileState(observer, phoneNumber);
}

class _ProfileState extends State<Profile>
    with SingleTickerProviderStateMixin, RouteAware {
  _ProfileState(this.observer, this.phoneNumber);
  final FirebaseAnalyticsObserver observer;
  final String phoneNumber;

  TabController _controller;
  int selectedIndex = 0;

  final List<Tab> tabs = <Tab>[
    const Tab(text: 'Productos'),
    const Tab(text: 'Perfil'),
  ];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller = TabController(
      vsync: this,
      length: tabs.length,
      initialIndex: selectedIndex,
    );
    _controller.addListener(() {
      setState(() {
        if (selectedIndex != _controller.index) {
          selectedIndex = _controller.index;
/*           _sendCurrentTabToAnalytics(); */
        }
      });
    });
  }

  Widget defaultContTabs() {
    return (DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.list)),
              Tab(icon: Icon(Icons.store)),
            ],
          ),
          title: Text('Tu Mercado'),
        ),
        body: TabBarView(
          children: [ProductsTab(observer), ProfileTab(observer, phoneNumber)],
        ),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return defaultContTabs();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    /*  observer.analytics.setCurrentScreen(
      screenName: '${Profile.routeName}/profile',
    ); */
  }
}
