import 'package:flutter/material.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'dart:io';
import 'package:image_picker/image_picker.dart';

import 'package:firebase_storage/firebase_storage.dart';

class ProfileTab extends StatefulWidget {
  static const String routeName = '/profiletab';

  ProfileTab(this.observer, this.phoneNumber);

  final FirebaseAnalyticsObserver observer;
  final String phoneNumber;

  @override
  State<StatefulWidget> createState() =>
      _ProfileTabState(observer, phoneNumber);
}

class _ProfileTabState extends State<ProfileTab>
    with SingleTickerProviderStateMixin, RouteAware {
  _ProfileTabState(this.observer, this.phoneNumber);

  final FirebaseAnalyticsObserver observer;
  final String phoneNumber;
  final descController = TextEditingController();
  Firestore fsinstance;

  int selectedIndex = 0;
  bool editDesc;
  bool editSeller;
  bool editPic;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    descController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    fsinstance = Firestore.instance;
    editDesc = false;
    editPic = false;
    editSeller = false;
    _sendCurrentTabToAnalytics();
  }

  List<Widget> buildDescription(DocumentSnapshot standInfoDocumentSnapshot) {
    if (editDesc == false) {
      return <Widget>[
        Text(
          "Sobre Nosotros: ",
          textAlign: TextAlign.left,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
        ),
        Container(
          child: Text(
            standInfoDocumentSnapshot["behindTheStand"],
            textAlign: TextAlign.left,
            softWrap: true,
          ),
        ),
        RaisedButton(
          child: new Icon(Icons.edit),
          onPressed: () {
            InternetAddress.lookup('google.com').then((result) {
              if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                setState(() {
                  editDesc = !editDesc;
                });
              }
            }).catchError((onError) {
              Scaffold.of(context).showSnackBar(
                  SnackBar(content: Text('Verifica tu conexión a internet')));
            });
          },
          color: Color.fromRGBO(255, 255, 255, 1.0),
          shape: CircleBorder(),
        ),
      ];
    }
    return <Widget>[
      Text(
        "Sobre Nosotros: ",
        textAlign: TextAlign.left,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      TextField(
        autocorrect: true,
        maxLength: 140,
        textAlign: TextAlign.left,
        controller: descController,
      ),
      RaisedButton(
        child: new Icon(Icons.done),
        onPressed: () {
          InternetAddress.lookup('google.com').then((result) {
            if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
              Firestore.instance.runTransaction((Transaction tx) async {
                DocumentSnapshot postSnapshot =
                    await tx.get(standInfoDocumentSnapshot.reference);
                if (postSnapshot.exists) {
                  await tx.update(standInfoDocumentSnapshot.reference,
                      <String, dynamic>{'behindTheStand': descController.text});

                  setState(() {
                    editDesc = !editDesc;
                  });
                }
              });
            }
          }).catchError((onError) {
            Scaffold.of(context).showSnackBar(
                SnackBar(content: Text('Verifica tu conexión a internet')));
          });
        },
      ),
    ];
  }

  Widget imageStack(
      DocumentSnapshot standInfoDocumentSnapshot, AsyncSnapshot userSnapshot) {
    return Stack(
      children: <Widget>[
        FadeInImage.memoryNetwork(
          placeholder: kTransparentImage,
          image: standInfoDocumentSnapshot['imageUrl'],
          width: 150.0,
          height: 150.0,
        ),
        Positioned(
          bottom: 0.0,
          right: 0.0,
          child: RaisedButton(
            child: new Icon(Icons.edit),
            onPressed: () async {
              InternetAddress.lookup('google.com').then((result) async {
                if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                  File imageFile =
                      await ImagePicker.pickImage(source: ImageSource.camera);
                  StorageReference ref = FirebaseStorage.instance
                      .ref()
                      .child('${userSnapshot.data.uid}.jpg');
                  StorageUploadTask uploadTask = ref.putFile(imageFile);

                  var storageSnapshot = await uploadTask.onComplete;
                  var downloadURL = await storageSnapshot.ref.getDownloadURL();

                  Firestore.instance.runTransaction((Transaction tx) async {
                    DocumentSnapshot postSnapshot =
                        await tx.get(standInfoDocumentSnapshot.reference);
                    if (postSnapshot.exists) {
                      await tx.update(standInfoDocumentSnapshot.reference,
                          <String, dynamic>{'imageUrl': downloadURL});
                      print('ya');
                    }
                  });
                }
              }).catchError((onError) {
                Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text('Verifica tu conexión a internet')));
              });
            },
            color: Color.fromRGBO(255, 255, 255, 1.0),
            shape: CircleBorder(),
          ),
        )
      ],
    );
  }

  Widget marketStandSellerColumn(DocumentSnapshot standInfoDocumentSnapshot) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text(
              "Mercado: ",
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
            FutureBuilder<DocumentSnapshot>(
                future: standInfoDocumentSnapshot['market'].get(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return new Text('Sin Conexión');
                    case ConnectionState.active:
                    case ConnectionState.waiting:
                      return new Text('Esperando datos...');
                    case ConnectionState.done:
                      if (snapshot.hasError)
                        return new Text('Error: ${snapshot.error}');
                      else
                        return new Container(
                            padding: EdgeInsets.all(5.0),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  snapshot.data['name'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                              ],
                            ));
                  }
                })
          ],
        ),
        Row(
          children: <Widget>[
            Text(
              "Stand:",
              textAlign: TextAlign.left,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
            ),
            Text(standInfoDocumentSnapshot["number"].toString())
          ],
        ),
        Row(
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  "Vendedor: ",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
                ),
                Text(standInfoDocumentSnapshot["sellers"][0]),
              ],
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            RaisedButton(
              child: new Icon(Icons.edit),
              onPressed: () {
                setState(() {});
              },
              color: Color.fromRGBO(255, 255, 255, 1.0),
              shape: CircleBorder(),
            ),
          ],
        )
      ],
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }

  @override
  Widget build(BuildContext context) {
    final FirebaseAuth auth = FirebaseAuth.instance;

    return FutureBuilder<FirebaseUser>(
        future: auth.currentUser(),
        builder: (BuildContext context, AsyncSnapshot userSnapshot) {
          switch (userSnapshot.connectionState) {
            case ConnectionState.none:
              return new Text('Sin Conexión');
            case ConnectionState.active:
            case ConnectionState.waiting:
              return new Text('Esperando datos...');
            case ConnectionState.done:
              if (userSnapshot.hasError)
                return new Text('Error: ${userSnapshot.error}');
              else
                return StreamBuilder<QuerySnapshot>(
                  stream: Firestore.instance
                      .collection('stand')
                      .where('owner',
                          isEqualTo: Firestore.instance
                              .collection('user')
                              .document(userSnapshot.data.uid))
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData)
                      return new Text('Cargando...');
                    else
                      return new ListView(
                        children: snapshot.data.documents
                            .map((DocumentSnapshot standInfoDocumentSnapshot) {
                          return new Container(
                              padding: EdgeInsets.all(10.0),
                              child: Column(children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    imageStack(standInfoDocumentSnapshot,
                                        userSnapshot),
                                    marketStandSellerColumn(
                                        standInfoDocumentSnapshot),
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Column(
                                      children: buildDescription(
                                          standInfoDocumentSnapshot),
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                    ),
                                  ],
                                )
                              ]));
                        }).toList(),
                      );
                  },
                );
          }
        });
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
/*     observer.analytics.setCurrentScreen(
      //CAMBIAR ESTA RUTA
      screenName: '${ProfileTab.routeName}',
    ); */
  }
}
