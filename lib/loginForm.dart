import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'profile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_analytics/observer.dart';
import 'dart:async';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'dart:io';

final FirebaseAuth _auth = FirebaseAuth.instance;

class LoginForm extends StatefulWidget {
  LoginForm(this.observer);

  final FirebaseAnalyticsObserver observer;

  @override
  State<StatefulWidget> createState() => LoginFormState(observer);
}

class LoginFormState extends State<LoginForm> {
  LoginFormState(this.observer);
  final FirebaseAnalyticsObserver observer;

  final _formKey1 = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  final phoneNumberController = TextEditingController();
  final codeController = TextEditingController();
  FocusNode textSecondFocusNode = new FocusNode();
  String verificationId;
  bool allowLogin;
  Map<String, double> _currentLocation;

  Location _location = new Location();
  String locError;
  String selectedMarket;
  @override
  void initState() {
    super.initState();
    allowLogin = false;
  } // Platform messages are asynchronous, so we initialize in an async method.

  initPlatformState() async {
    Map<String, double> location;
    // Platform messages may fail, so we use a try/catch PlatformException.

    try {
      location = await _location.getLocation();
      locError = null;
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        locError = 'Permiso de locación denegado';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        locError =
            'No tenemos permisos de locación. Por favor habilítelos en Ajustes';
      }
      location = null;
    }
    setState(() {});
  }

  Future<void> _testSignInWithPhoneNumber() async {
    final FirebaseUser user = await _auth.signInWithPhoneNumber(
      verificationId: verificationId,
      smsCode: codeController.text,
    );

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);
  }

  Future<void> _testVerifyPhoneNumber() async {
    final PhoneVerificationCompleted verificationCompleted =
        (FirebaseUser user) {
      print('signInWithPhoneNumber auto succeeded: $user');
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      print(
          'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      this.verificationId = verificationId;
    };

    print('yayaya1');
    await _auth.verifyPhoneNumber(
        phoneNumber: "+57" + phoneNumberController.text,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Function allowLoginButton() {
    FocusScope.of(context).requestFocus(textSecondFocusNode);

    if (allowLogin == true) {
      return () {
        if (_formKey2.currentState.validate()) {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text('Procesando...')));
          _testSignInWithPhoneNumber().then((s) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        Profile(observer, phoneNumberController.text)));
          }).catchError((err) {
            print(err);
            Scaffold.of(context).showSnackBar(
                SnackBar(content: Text('Código erróneo, intenta de nuevo')));
          });
        }
      };
    }
    return null;
  }

  Widget selectMarket() {
    List markets = new List();
    markets.add(["Paloquemao", 4.616131, -74.083647]);
    markets.add(["Codabas", 4.7570053, -74.0287205]);
    markets.add(["Corabastos", 4.632295, -74.153881]);

    if (locError != null) {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(locError)));
    }
    if (_currentLocation != null) {
      double longDiff;
      double latDiff;
      String recommended = "Seleccione un mercado";
      for (final market in markets) {
        longDiff = market[2] - _currentLocation["longitude"];
        latDiff = market[1] - _currentLocation["latitude"];
        if (latDiff.abs() < 0.0001 && longDiff.abs() < 0.0001) {
          recommended = market[0];
          selectedMarket = recommended;
        }
      }
      return DropdownButton<String>(
        items:
            <String>['Paloquemao', 'Codabas', 'Corabastos'].map((String value) {
          return new DropdownMenuItem<String>(
            value: value,
            child: new Text(value),
          );
        }).toList(),
        onChanged: (changedVal) {
          selectedMarket = changedVal;
        },
        hint: Text(recommended),
        value: selectedMarket,
      );
    }
    return DropdownButton<String>(
      items:
          <String>['Paloquemao', 'Corabastos', 'Codabas'].map((String value) {
        return new DropdownMenuItem<String>(
          value: value,
          child: new Text(value),
        );
      }).toList(),
      onChanged: (changedVal) {
        selectedMarket = changedVal;
      },
      hint: Text("Central de Abastos"),
    );
  }

  Widget codeForm() {
    if (allowLogin == true) {
      return (Form(
        key: _formKey2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            TextFormField(
              inputFormatters: [LengthLimitingTextInputFormatter(6)],
              keyboardType: TextInputType.number,
              decoration: InputDecoration(labelText: "Código"),
              controller: codeController,
              focusNode: textSecondFocusNode,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Este campo no puede estar vacío';
                }
              },
            ),
            Center(
              child: RaisedButton(
                child: Text('Siguiente'),
                onPressed: allowLoginButton(),
              ),
            ),
          ],
        ),
      ));
    }
    return Text("Recuerda que tu código puede demorarse un par de minutos");
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        children: <Widget>[
          Form(
            key: _formKey1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                TextFormField(
                  inputFormatters: [LengthLimitingTextInputFormatter(1044545)],
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(labelText: "Número de Stand"),
                  controller: phoneNumberController,
                  onSaved: (String value) {
                    print(value);
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Este campo no puede estar vacío';
                    }
                  },
                ),
                Center(
                  child: RaisedButton(
                    child: Text('Generar código'),
                    onPressed: () {
                      InternetAddress.lookup('google.com').then((result) {
                        if (result.isNotEmpty &&
                            result[0].rawAddress.isNotEmpty) {
                          print('connected');
                          if (_formKey1.currentState.validate()) {
                            _testVerifyPhoneNumber();
                            Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(
                                    'Recibirás un mensaje de texto con el código de ingreso')));
                            setState(() {
                              allowLogin = true;
                            });
                          }
                        }
                      }).catchError((onError) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(
                                    'Verifica tu conexión a internet')));
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          codeForm()
        ],
      ),
    );
  }
}
