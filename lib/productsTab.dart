import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:io';

class ProductsTab extends StatefulWidget {
  static const String routeName = '/productstab';

  ProductsTab(this.observer);

  final FirebaseAnalyticsObserver observer;

  @override
  State<StatefulWidget> createState() => _ProductsTabState(observer);
}

class _ProductsTabState extends State<ProductsTab>
    with SingleTickerProviderStateMixin, RouteAware {
  _ProductsTabState(this.observer);

  String _newProduct;

  final FirebaseAnalyticsObserver observer;
  TabController _controller;
  int selectedIndex = 0;

  final List<Tab> tabs = <Tab>[
    const Tab(text: 'Productos'),
    const Tab(text: 'Perfil'),
  ];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _newProduct = null;

    _controller = TabController(
      vsync: this,
      length: tabs.length,
      initialIndex: selectedIndex,
    );
    _controller.addListener(() {
      setState(() {
        if (selectedIndex != _controller.index) {
          selectedIndex = _controller.index;
          _sendCurrentTabToAnalytics();
        }
      });
    });
  }

  Widget reduceQuantityButton(DocumentSnapshot productInStandDocumentSnapshot) {
    return RaisedButton(
      child: Text('-'),
      color: Color.fromRGBO(255, 0, 0, 1.0),
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0)),
      onPressed: () {
        InternetAddress.lookup('google.com').then((result) {
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            if (productInStandDocumentSnapshot['quantity'] != 0) {
              Firestore.instance.runTransaction((Transaction tx) async {
                DocumentSnapshot postSnapshot =
                    await tx.get(productInStandDocumentSnapshot.reference);
                if (postSnapshot.exists) {
                  await tx.update(
                      productInStandDocumentSnapshot.reference,
                      <String, dynamic>{
                        'quantity': postSnapshot.data['quantity'] - 1
                      });
                }
              });
            }
          }
        }).catchError((onError) {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('Verifica tu conexión a internet')));
        });
      },
    );
  }

  Widget increaseQuantityButton(
      DocumentSnapshot productInStandDocumentSnapshot) {
    return RaisedButton(
      child: Text('+'),
      color: Color.fromRGBO(0, 255, 0, 1.0),
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0)),
      onPressed: () {
        InternetAddress.lookup('google.com').then((result) {
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            Firestore.instance.runTransaction((Transaction tx) async {
              DocumentSnapshot postSnapshot =
                  await tx.get(productInStandDocumentSnapshot.reference);
              if (postSnapshot.exists) {
                await tx.update(
                    productInStandDocumentSnapshot.reference, <String, dynamic>{
                  'quantity': postSnapshot.data['quantity'] + 1
                });
              }
            });
          }
        }).catchError((onError) {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('Verifica tu conexión a internet')));
        });
      },
    );
  }

  Widget deleteButton(DocumentSnapshot productInStandDocumentSnapshot) {
    return RaisedButton(
      child: Text('Eliminar'),
      color: Color.fromRGBO(255, 0, 0, 1.0),
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0)),
      onPressed: () {
        InternetAddress.lookup('google.com').then((result) {
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            Firestore.instance.runTransaction((Transaction tx) async {
              DocumentSnapshot postSnapshot =
                  await tx.get(productInStandDocumentSnapshot.reference);
              if (postSnapshot.exists) {
                await tx.delete(productInStandDocumentSnapshot.reference);
              }
            });
          }
        }).catchError((onError) {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('Verifica tu conexión a internet')));
        });
      },
    );
  }

  Widget enabledCheckbox(DocumentSnapshot productInStandDocumentSnapshot) {
    return Checkbox(
      value: productInStandDocumentSnapshot['isEnabled'],
      onChanged: (val) {
        Firestore.instance.runTransaction((Transaction tx) async {
          DocumentSnapshot postSnapshot =
              await tx.get(productInStandDocumentSnapshot.reference);
          if (postSnapshot.exists) {
            await tx.update(productInStandDocumentSnapshot.reference,
                <String, dynamic>{'isEnabled': val});
          }
        });
      },
    );
  }

  List<Widget> imageAndNameList(
      AsyncSnapshot productSnapshot, DocumentSnapshot productInStandSnapshot) {
    return ([
      Text(
        productSnapshot.data['name'],
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 22.0,
        ),
      ),
      deleteButton(productInStandSnapshot),
      FadeInImage.memoryNetwork(
        width: 200.0,
        height: 200.0,
        placeholder: kTransparentImage,
        image: productSnapshot.data['image'],
      ),
    ]);
  }

  Widget addProductButton() {
    return RaisedButton(
      child: Text('Agregar producto'),
      onPressed: () {
        InternetAddress.lookup('google.com').then((result) {
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            _newProductModal();
          }
        }).catchError((onError) {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('Verifica tu conexión a internet')));
        });
      },
    );
  }

  Widget productColumn(DocumentSnapshot productInStandSnapshot) {
    return Column(
      children: <Widget>[
        FutureBuilder<DocumentSnapshot>(
            future: productInStandSnapshot['product'].get(),
            builder: (BuildContext context, AsyncSnapshot productSnapshot) {
              switch (productSnapshot.connectionState) {
                case ConnectionState.none:
                  return new Text('Sin Conexión');
                case ConnectionState.active:
                case ConnectionState.waiting:
                  return new Text('Esperando datos...');
                case ConnectionState.done:
                  if (productSnapshot.hasError)
                    return new Text('Error: ${productSnapshot.error}');
                  else
                    return new Container(
                        padding: EdgeInsets.all(5.0),
                        child: Column(
                          children: imageAndNameList(
                              productSnapshot, productInStandSnapshot),
                        ));
              }
            }),
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text("Disponible"),
                    enabledCheckbox(productInStandSnapshot),
                  ],
                ),
                reduceQuantityButton(productInStandSnapshot),
                Column(
                  children: <Widget>[
                    Text("Cantidad"),
                    Text(
                      '${productInStandSnapshot['quantity']}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
                increaseQuantityButton(productInStandSnapshot),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ],
        ),
      ],
    );
  }

  Widget showAllProducts() {
    return (StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('productInStand').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) {
          return new Text('Cargando...');
        } else {
          return new Expanded(
              child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.all(20.0),
            children: snapshot.data.documents
                .map((DocumentSnapshot productInStandDocumentSnapshot) {
              return new Container(
                  padding: EdgeInsets.all(10.0),
                  child: productColumn(productInStandDocumentSnapshot));
            }).toList(),
          ));
        }
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[addProductButton(), showAllProducts()],
    );
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
/*     observer.analytics.setCurrentScreen(
      //CAMBIAR ESTA RUTA
      screenName: '${ProductsTab.routeName}/productstab',
    ); */
  }

  Future<Null> _newProductModal() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Agrega tu producto'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                StreamBuilder<QuerySnapshot>(
                  stream: Firestore.instance.collection('product').snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData)
                      return new Text('Cargando...');
                    else {
                      return new DropdownButton(
                        value: _newProduct,
                        hint: Text('Selecciona uno de la lista'),
                        items: snapshot.data.documents.map(
                            (DocumentSnapshot productInStandDocumentSnapshot) {
                          return new DropdownMenuItem(
                              value: productInStandDocumentSnapshot.documentID,
                              child:
                                  Text(productInStandDocumentSnapshot['name']));
                        }).toList(),
                        onChanged: (String newValue) {
                          setState(() {
                            _newProduct = newValue;
                          });
                        },
                      );
                    }
                  },
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              textColor: Color.fromRGBO(255, 255, 255, 1.0),
              color: Color.fromRGBO(255, 0, 0, 1.0),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Aceptar'),
              color: Color.fromRGBO(0, 255, 0, 1.0),
              textColor: Color.fromRGBO(0, 0, 0, 1.0),
              onPressed: () {
                InternetAddress.lookup('google.com').then((result) {
                  if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                    Firestore.instance.runTransaction((Transaction tx) async {
                      final FirebaseAuth auth = FirebaseAuth.instance;
                      var user = await auth.currentUser();

                      var userReference = Firestore.instance
                          .collection('user')
                          .document(user.uid);

                      var stands = await Firestore.instance
                          .collection('stand')
                          .where('owner', isEqualTo: userReference)
                          .getDocuments();
                      var stand = stands.documents[0];

                      var productsInStand = await Firestore.instance
                          .collection('productInStand')
                          .where('stand', isEqualTo: stand.reference)
                          .getDocuments();
                      var exists = false;

                      var productInStand;
                      for (var i = 0;
                          i < productsInStand.documents.length;
                          i++) {
                        productInStand = productsInStand.documents[i];
                        if (productInStand['product'].documentID == _newProduct)
                          exists = true;
                      }

                      if (!exists) {
                        await Firestore.instance
                            .collection('productInStand')
                            .add({
                          'isEnabled': true,
                          'product': Firestore.instance
                              .collection('product')
                              .document(_newProduct),
                          'quantity': 0,
                          'stand': stand.reference
                        });
                      }
                      Navigator.of(context).pop();
                    });
                  }
                }).catchError((onError) {
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Verifica tu conexión a internet')));
                });
              },
            )
          ],
        );
      },
    );
  }
}
